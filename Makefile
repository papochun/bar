CC = c99
CFLAGS = -std=c99 -pedantic -Wall -Wno-deprecated-declarations -Os
PREFIX = /usr/local/bin

bar: main.c
	$(CC) -o bar main.c $(CFLAGS)

clean:
	rm -f bar

install: bar
	mkdir -p $(PREFIX)
	cp -f bar $(PREFIX)
	cp -f script/bar-dwm $(PREFIX)
	cp -f script/bar-lemonbar $(PREFIX)
	chmod 755 $(PREFIX)/bar
	chmod 755 $(PREFIX)/bar-dwm
	chmod 755 $(PREFIX)/bar-lemonbar

uninstall:
	rm -f $(PREFIX)/bar
	rm -f $(PREFIX)/bar-dwm
	rm -f $(PREFIX)/bar-lemonbar
