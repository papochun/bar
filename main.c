#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <time.h>
#include <sys/statvfs.h>
#include <sys/stat.h>

/* character size of battery file path, usually 128 bytes would suffice */
#define MAX_PATH 128

/* uses a struct to return a character array */
struct string {
	char s[8 * MAX_PATH];
};

/* takes line number, word count, and filename, and outputs a word  */
struct string read_word(int line, int word, char *file) {
	struct string buffer, result;

	if (access(file, F_OK) != 0) {
		result.s[0]='\0';
		return result;
	}

	FILE *fp = fopen(file, "r");
	for (int i = 1; i <= line; i++) {
		if (fgets(buffer.s, sizeof(buffer.s), fp) == NULL)
			exit(1);
	}
	fclose(fp);

	strncpy(result.s, strtok(buffer.s, " "), sizeof(result.s));
	for (int i = 1; i != word; i++) {
		strncpy(result.s, strtok(NULL, " "), sizeof(result.s));
		if (result.s[0] == '\0') {
			printf("%s\n","failed");
			exit(1);
		}
	}

	if (result.s[strlen(result.s)-1] == '\n')
		result.s[strlen(result.s)-1] = '\0';

	return result;
}

/* returns battery percents for each device connected */
struct string battery() {
	struct string battery, path;
	struct dirent *files;
	
	battery.s[0] = '\0';

	DIR *dp = opendir("/sys/class/power_supply");
	if (dp == NULL) {
		strncpy(battery.s, "BT", MAX_PATH);
		return battery;
	}
	
	while ((files = readdir(dp)) != NULL) {
		if (
			!strcmp(files->d_name, ".") ||
			!strcmp(files->d_name, "..")
		) continue;

		strncpy(path.s, "/sys/class/power_supply/", MAX_PATH);
		strncat(path.s, files->d_name, MAX_PATH);
		strncat(path.s, "/capacity", MAX_PATH);

		if (access(path.s, F_OK) == 0) {
			if (battery.s[0] != '\0')
				strncat(battery.s, " ", MAX_PATH);

			strncat(battery.s, read_word(1, 1, path.s).s, MAX_PATH);
			strncat(battery.s, "%", MAX_PATH);
		}
	}
	closedir(dp);

	if (battery.s[0] == '\0')
		strncpy(battery.s, "BT", MAX_PATH);
	return battery;
}

/* returns the 1 minute load average */
struct string load() {
		struct string load;
		strcpy(load.s, read_word(1, 2, "/proc/loadavg").s);
		return load;
}

/* returns the actively used memory usage, should be accurate */
unsigned int memory() {
	const unsigned int total = atoi(read_word(1, 2, "/proc/meminfo").s);
	const unsigned int free = atoi(read_word(2, 2, "/proc/meminfo").s);
	const unsigned int buffer = atoi(read_word(4, 2, "/proc/meminfo").s);
	const unsigned int cached = atoi(read_word(5, 2, "/proc/meminfo").s);
	const unsigned int slab = atoi(read_word(24, 2, "/proc/meminfo").s);

	return (total - free - buffer - cached - slab) / 1000;
}

/* returns the disk usage in GigaBytes on the drive mounted to / */
struct string disk() {
	struct statvfs buffer;
	struct string result;

	statvfs("/", &buffer);

	const double GB = (1024 * 1024) * 1024;
	const double total = (buffer.f_blocks * buffer.f_frsize) / GB;
	const double available = (buffer.f_bfree * buffer.f_frsize) / GB;
	const double used = total - available;

	sprintf(result.s, "%0.fGb",used);
	return result;
}

/* returns the date and time in that order */
struct string timedate() {
	struct string temp, var;
	const time_t rawtime = time(NULL);

	var.s[0] = '\0';
	strftime(temp.s, sizeof(temp.s), "%a %d %b  [%H:%M:%S]", localtime(&rawtime));
	strcat(var.s, temp.s);
	temp.s[0] = '\0';

	if (var.s[0] != '\0')
		strncat(var.s, "  ", sizeof(var.s));

	strftime(temp.s, sizeof(temp.s), "%H:%M:%S", gmtime(&rawtime));
	strcat(var.s, temp.s);
	temp.s[0] = '\0';

	return var;
}

int main() {
        printf("%s  %s  %dMb  %s  %s\n", battery().s, load().s, memory(), disk().s, timedate().s);
}
